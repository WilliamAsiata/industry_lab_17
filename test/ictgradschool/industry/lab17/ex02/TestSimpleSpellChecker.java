package ictgradschool.industry.lab17.ex02;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * TODO Implement this class.
 */
public class TestSimpleSpellChecker {

    SimpleSpellChecker spellChecker;

    @Before
    public void setUp() {
        try {
            spellChecker = new SimpleSpellChecker(new Dictionary(),"The quick brown fox jumps over the lazy dog. Da kwik beroun ph0x gumps 0va da layz dawg.");
        } catch (InvalidDataFormatException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSpellCheckerConstruction() {
        assertTrue(spellChecker != null);
    }
    @Test
    public void testGetMisspelledWords() {
        // Test for words not in dictionary (or mispelled), excluding words beginning with numbers.
        String words = "[gumps, dawg, layz, lazy, beroun, jumps, fox, ph0x, 0va, kwik, da]";
        assertEquals(words,spellChecker.getMisspelledWords().toString());
    }

    @Test
    public void testGetUniqueWords() {
        // Test for 16 unique words in the provided string of 18 words
        String words = "[over, gumps, quick, dawg, layz, lazy, beroun, jumps, brown, fox, the, ph0x, 0va, kwik, dog, da]";
        assertEquals(words,spellChecker.getUniqueWords().toString());
    }

    @Test
    public void testGetFrequencyOfWord() {
        int freq = 0;
        try {
            freq = spellChecker.getFrequencyOfWord("da");
        } catch (InvalidDataFormatException e) {
            fail();
            e.printStackTrace();
        }
        assertEquals(2,freq);
    }
}