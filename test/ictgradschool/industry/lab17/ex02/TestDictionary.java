package ictgradschool.industry.lab17.ex02;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * TODO Implement this class.
 */
public class TestDictionary {

    private IDictionary dictionary;

    @Before
    public void setUp() {
        dictionary = new Dictionary();
    }

    @Test
    public void testIDictionaryConstruction() {
        assertTrue(dictionary.WORDS != null);
    }
    @Test
    public void testWordNotInDictionary() {
        // Ensure spell check returns false for mis-spelled words
        assertFalse(dictionary.isSpellingCorrect("jgr5654xhdx"));
    }

    @Test
    public void testWordInDictionary() {
        // Ensure spell check returns true for correct words
        assertTrue(dictionary.isSpellingCorrect("the"));
    }
}