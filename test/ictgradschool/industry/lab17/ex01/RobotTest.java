package ictgradschool.industry.lab17.ex01;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RobotTest {

    private Robot myRobot;

    @Before
    public void setUp() {
        myRobot = new Robot();
    }

    @Test
    public void testRobotConstruction() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void testIllegalMoveNorth() {
        boolean atTop = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atTop = myRobot.currentState().row == 1;
            assertTrue(atTop);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().row);
        }
    }

    @Test
    public void testTurn() {

        // Turn robot right from North to East
        myRobot.turn();
        assertEquals(Robot.Direction.East, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());

        // Turn robot right from East to South
        myRobot.turn();
        assertEquals(Robot.Direction.South, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());

        // Turn robot right from South to West
        myRobot.turn();
        assertEquals(Robot.Direction.West, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());

        // Turn robot right from West to North
        myRobot.turn();
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void testMove() {

        for (int i = 0; i < 4; i++){

            Robot.Direction direction = myRobot.getDirection();
            try {
                // Move the robot to the next corner edge.
                for (int j = 0; j < Robot.GRID_SIZE - 1; j++)
                    myRobot.move();

                // Check that robot has reached the edge.
                switch (direction){
                    case North:
                        assertTrue(myRobot.row() == 1 && myRobot.column() == 1);
                        break;
                    case East:
                        assertTrue(myRobot.row() == 1 && myRobot.column() == 10);
                        break;
                    case South:
                        assertTrue(myRobot.row() == 10 && myRobot.column() == 10);
                        break;
                    case West:
                        assertTrue(myRobot.row() == 10 && myRobot.column() == 1);
                }
            } catch (IllegalMoveException e) {
                fail();
            }

            Robot.RobotState edgeState = myRobot.currentState();

            try {
                // Now try to continue to move.
                myRobot.move();
                fail();
            } catch (IllegalMoveException e) {
                // Ensure the move didn’t change the robot state
                assertEquals(edgeState.row, myRobot.currentState().row);
                assertEquals(edgeState.column, myRobot.currentState().column);
            }

            myRobot.turn();
        }
    }
}
